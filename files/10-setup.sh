#!/bin/bash
#
# Install additional packages and setup user environment
#

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


readonly TZ=${TIMEZONE}

echo "### Setting timezone to ${TZ}"

# Timezone
echo "${TZ}" > /etc/timezone
ln -svf /usr/share/zoneinfo/${TZ} /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

echo "### Improving privacy"

apt-get --yes remove apport popularity-contest whoopsie

echo "### Installing packages"

apt-get --yes install jq
