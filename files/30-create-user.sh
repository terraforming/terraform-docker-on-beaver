#!/bin/bash
#
# Create default user
#

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


echo "### Creating default user"

readonly USER=ubuntu
readonly PASS=${USER_PASS}

if ! id ${USER} >/dev/null 2>&1; then
    adduser --disabled-password --gecos "" ${USER}
fi

echo "${USER}:${PASS}" | chpasswd

usermod --append --groups sudo ${USER}
# Remove the configuration from cloud-init (passwordless sudo)
rm -f /etc/sudoers.d/90-cloud-init-users

usermod --append --groups docker ${USER}

rsync --archive --chown=${USER}:${USER} ~/.ssh /home/${USER}
chmod 400 /home/${USER}/.ssh/authorized_keys
chattr +i /home/${USER}/.ssh/authorized_keys
chattr +i /home/${USER}/.ssh/
