#!/bin/bash
#
# Configure SSHD (change listening port and harden)
#

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


readonly PORT=${SSHD_PORT}

echo "### Configuring SSHD to port ${PORT}"

sed --in-place "s/^[# ]*Port.*$/Port ${PORT}/g" /etc/ssh/sshd_config
sed --in-place "s/^[# ]*PermitRootLogin.*$/PermitRootLogin no/g" /etc/ssh/sshd_config
sed --in-place "s/^[# ]*PasswordAuthentication.*$/PasswordAuthentication no/g" /etc/ssh/sshd_config

# Do not restart sshd explicity to allow remote-exec from root module
#systemctl restart sshd
