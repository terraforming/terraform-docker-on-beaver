#!/bin/bash
#
# Upgrade all installed packages
#

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


echo "### Upgrading packages"

# Upgrade and dist-upgrade
apt-get --yes update
DEBIAN_FRONTEND=noninteractive apt-get --yes --option Dpkg::Options::="--force-confdef" --option Dpkg::Options::="--force-confold" upgrade
DEBIAN_FRONTEND=noninteractive apt-get --yes --option Dpkg::Options::="--force-confdef" --option Dpkg::Options::="--force-confold" dist-upgrade
apt-get --yes autoremove
