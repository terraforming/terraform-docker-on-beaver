#!/bin/bash
#
# Install Docker
#

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


echo "### Installing Docker"

# https://docs.docker.com/install/linux/docker-ce/ubuntu/
apt-get --yes update
apt-get --yes install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release --codename --short) stable"
apt-get --yes update
apt-get --yes install docker-ce


echo "### Installing Docker Compose"

# https://docs.docker.com/compose/install/#install-compose
readonly RELEASE=$(curl --silent https://api.github.com/repos/docker/compose/releases/latest | jq --raw-output '.tag_name')
curl --silent --location https://github.com/docker/compose/releases/download/${RELEASE}/docker-compose-$(uname --kernel-name)-$(uname --machine) --output /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version
