#!/bin/bash

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


_setup_logging() {
    local user=$(id --user --name)
    local group=$(id --group --name)
    local logfile=/var/log/terraform-docker-on-beaver.log
    sudo touch ${logfile}
    sudo chown ${user}.${group} ${logfile}
    exec > >(tee --append --ignore-interrupts ${logfile}) 2>&1
}

_setup_logging

main() {
    echo "# $(basename ${0}) - Started at $(date)"

    export TIMEZONE=${1}; shift
    export USER_PASS=${1}; shift
    export SSHD_PORT=${1}; shift
    export REBOOT=${1}; shift

    for f in $(ls | egrep "^[0-9]+.*\.sh$" | sort --numeric-sort); do
        echo "## Running script $(pwd)/${f}"
        ./${f}
    done

    if [[ ${REBOOT} == "true" ]]; then
        echo "## Reboot"
        shutdown --reboot +1
    fi

    echo "# $(basename ${0}) - Finished at $(date)"
}

[[ "${BASH_SOURCE[0]}" != "${@}" ]] && main "${@}"
