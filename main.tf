data "local_file" "00_upgrade_sh" {
  filename = "${path.module}/files/00-upgrade.sh"
}

data "local_file" "10_setup_sh" {
  filename = "${path.module}/files/10-setup.sh"
}

data "local_file" "20_install_docker_sh" {
  filename = "${path.module}/files/20-install-docker.sh"
}

data "local_file" "30_create_user_sh" {
  filename = "${path.module}/files/30-create-user.sh"
}

data "local_file" "40_configure_sshd_sh" {
  filename = "${path.module}/files/40-configure-sshd.sh"
}

data "local_file" "terraform_docker_on_beaver_sh" {
  filename = "${path.module}/files/terraform-docker-on-beaver.sh"
}
