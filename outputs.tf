output "scripts" {
  value = [
    "${data.local_file.00_upgrade_sh.content}",
    "${data.local_file.10_setup_sh.content}",
    "${data.local_file.20_install_docker_sh.content}",
    "${data.local_file.30_create_user_sh.content}",
    "${data.local_file.40_configure_sshd_sh.content}",
  ]
}

output "launcher" {
  value = "${data.local_file.terraform_docker_on_beaver_sh.content}"
}
